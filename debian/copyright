Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: qelectrotech
Upstream-Contact: Laurent Trinques <scorpio@qelectrotech.org>
Source: https://download.tuxfamily.org/qet/tags/

Files: *
Copyright: Benoît Ansieau       <benoit@qelectrotech.org>
           Laurent Trinques     <scorpio@qelectrotech.org>
           Joshua claveau       <joshua_39@live.fr>
           Cyril frausty        <cyril.frausti@gmail.com>
           Xavier Guerrin       <xavier@qelectrotech.org>
           Youssef Oualmakran   <youssefsan@qelectrotech.org>
License: GPL-2+

Files: sources/richtext/*
Copyright: 2013 Digia Plc and/or its subsidiary(-ies).
License: LGPL-2.1 with Digia exception

Files: elements/*
Copyright: The QElectroTech team
License: CC-BY-3.0

Files: ico/24x16/*
Copyright: Damien Sorel
License: public-domain
	24x16 Flag icons - http://www.strangeplanet.fr
	These icons are public domain, and as such are free for any use (attribution appreciated but not required).
	Note that these flags are mostly named using the ISO3166-1 alpha-2 country codes where appropriate.
	If you redistribute the pack please let the readme file into.
	If you find these icons useful, share-them!
	Contact: webmaster@strangeplanet.fr

Files: ico/128x128/settings.png
       ico/16x16/application-exit.png
       ico/16x16/arrow-*.png
       ico/16x16/configure.png
       ico/16x16/configure-toolbars.png
       ico/16x16/dialog-cancel.png
       ico/16x16/dialog-ok.png
       ico/16x16/document-*.png
       ico/16x16/edit-*.png
       ico/16x16/folder-new.png
       ico/16x16/folder.png
       ico/16x16/go-home.png
       ico/16x16/object-rotate-right.png
       ico/16x16/user-*.png
       ico/16x16/view-*.png
       ico/16x16/window-new.png
       ico/16x16/zoom-*.png
       ico/16x16/go-down.png
       ico/16x16/go-up.png
       ico/16x16/preferences-desktop-user.png
       ico/16x16/text-xml.png
       ico/22x22/go-down.png
       ico/22x22/go-up.png
       ico/22x22/application-exit.png
       ico/22x22/arrow-*.png
       ico/22x22/configure.png
       ico/22x22/configure-toolbars.png
       ico/22x22/dialog-*.png
       ico/22x22/document-*.png
       ico/22x22/edit-*.png
       ico/22x22/go-home.png
       ico/22x22/object-*.png
       ico/22x22/start.png
       ico/22x22/view*.png
       ico/22x22/window-new.png
       ico/22x22/zoom-*.png
       ico/22x22/preferences-desktop-user.png
       ico/22x22/text-xml.png
       ico/32x32/application-pdf.png
       ico/32x32/image-x-eps.png
       ico/32x32/printer.png
       ico/32x32/text-xml.png
       ico/128x128/document-export.png
       ico/128x128/printer.png
Copyright: 2009, Nuno Pinheiro
License: LGPL-2.1

Files: ico/16x16/circle.png
       ico/16x16/masquer.png
       ico/22x22/arc.png
       ico/22x22/bring_forward.png
       ico/22x22/ellipse.png
       ico/22x22/line.png
       ico/22x22/lower.png
       ico/22x22/move.png
       ico/22x22/names.png
       ico/22x22/polygon.png
       ico/32x32/qt.png
       ico/22x22/raise.png
       ico/22x22/rectangle.png
       ico/22x22/restaurer.png
       ico/22x22/select.png
       ico/22x22/send_backward.png
       ico/22x22/textfield.png
       ico/22x22/text.png
Copyright: 2009, Everaldo Coelho
License: LGPL-2.1

Files: ico/breeze-icons/*
Copyright: 2014, Uri Herrera <uri_herrera@nitrux.in>
License: LGPL-3

Files: debian/*
Copyright: 2011-2015, Laurent Trinques <scorpio@qelectrotech.org>
           2011-2015, Denis Briand <debian@denis-briand.fr>
License: GPL-2+

License: LGPL-2.1 with Digia exception
	Commercial License Usage
	Licensees holding valid commercial Qt licenses may use this file in
	accordance with the commercial license agreement provided with the
	Software or, alternatively, in accordance with the terms contained in
	a written agreement between you and Digia.  For licensing terms and
	conditions see http://qt.digia.com/licensing.  For further information
	use the contact form at http://qt.digia.com/contact-us.
	.
	GNU Lesser General Public License Usage
	Alternatively, this file may be used under the terms of the GNU Lesser
	General Public License version 2.1 as published by the Free Software
	Foundation and appearing in the file LICENSE.LGPL included in the
	packaging of this file.  Please review the following information to
	ensure the GNU Lesser General Public License version 2.1 requirements
	will be met: http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html.
	.
	In addition, as a special exception, Digia gives you certain additional
	rights.  These rights are described in the Digia Qt LGPL Exception
	version 1.1, included in the file LGPL_EXCEPTION.txt in this package.
	.
	GNU General Public License Usage
	Alternatively, this file may be used under the terms of the GNU
	General Public License version 3.0 as published by the Free Software
	Foundation and appearing in the file LICENSE.GPL included in the
	packaging of this file.  Please review the following information to
	ensure the GNU General Public License version 3.0 requirements will be
	met: http://www.gnu.org/copyleft/gpl.html.

License: CC-BY-3.0
	The elements collection provided along with QElectroTech is provided as is and
	without any warranty of fitness for your purpose or working.
	The usage, the modification and the integration of the elements into electric
	diagrams is allowed without any condition, whatever the final license of the
	diagrams is.
	If you redistribute all or a part of the QElectroTech collection, with or
	without any modification, out of an electric diagram, you must respect the
	conditions of the CC-BY license:
	This work is licensed under the Creative Commons Attribution 3.0 License.
	To view a copy of this license, visit
	http://creativecommons.org/licenses/by/3.0/ or send a letter to Creative
	Commons, 171 Second Street, Suite 300, San Francisco, California, 94105, USA.

License: LGPL-2.1
	This program is free software; you can redistribute it and/or
	modify it under the terms of the GNU Lesser General Public
	License as published by the Free Software Foundation; either
	version 2.1 of the License, or (at your option) any later version.
	.
	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
	Lesser General Public License for more details.
	.
	You should have received a copy of the GNU Lesser General Public
	License along with this program; if not, write to the Free Software
	Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
	.
	On Debian systems, the complete text of the GNU Lesser General Public
	License, can be found in `/usr/share/common-licenses/LGPL-2.1'.

License: LGPL-3
		   GNU LESSER GENERAL PUBLIC LICENSE
                       Version 3, 29 June 2007
	.
	Copyright (C) 2007 Free Software Foundation, Inc. <http://fsf.org/>
	Everyone is permitted to copy and distribute verbatim copies
	of this license document, but changing it is not allowed.
	.
	.
	  This version of the GNU Lesser General Public License incorporates
	the terms and conditions of version 3 of the GNU General Public
	License, supplemented by the additional permissions listed below.
	.
	  0. Additional Definitions.
	.
	  As used herein, "this License" refers to version 3 of the GNU Lesser
	General Public License, and the "GNU GPL" refers to version 3 of the GNU
	General Public License.
	.
	  "The Library" refers to a covered work governed by this License,
	other than an Application or a Combined Work as defined below.
	.
	  An "Application" is any work that makes use of an interface provided
	by the Library, but which is not otherwise based on the Library.
	Defining a subclass of a class defined by the Library is deemed a mode
	of using an interface provided by the Library.
	.
	  A "Combined Work" is a work produced by combining or linking an
	Application with the Library.  The particular version of the Library
	with which the Combined Work was made is also called the "Linked
	Version".
	.
	  The "Minimal Corresponding Source" for a Combined Work means the
	Corresponding Source for the Combined Work, excluding any source code
	for portions of the Combined Work that, considered in isolation, are
	based on the Application, and not on the Linked Version.
	.
	  The "Corresponding Application Code" for a Combined Work means the
	object code and/or source code for the Application, including any data
	and utility programs needed for reproducing the Combined Work from the
	Application, but excluding the System Libraries of the Combined Work.
	.
	  1. Exception to Section 3 of the GNU GPL.
	.
	  You may convey a covered work under sections 3 and 4 of this License
	without being bound by section 3 of the GNU GPL.
	.
	  2. Conveying Modified Versions.
	.
	  If you modify a copy of the Library, and, in your modifications, a
	facility refers to a function or data to be supplied by an Application
	that uses the facility (other than as an argument passed when the
	facility is invoked), then you may convey a copy of the modified
	version:
	.
	   a) under this License, provided that you make a good faith effort to
	   ensure that, in the event an Application does not supply the
	   function or data, the facility still operates, and performs
	   whatever part of its purpose remains meaningful, or
	.
	   b) under the GNU GPL, with none of the additional permissions of
	   this License applicable to that copy.
	.
	  3. Object Code Incorporating Material from Library Header Files.
	.
	  The object code form of an Application may incorporate material from
	a header file that is part of the Library.  You may convey such object
	code under terms of your choice, provided that, if the incorporated
	material is not limited to numerical parameters, data structure
	layouts and accessors, or small macros, inline functions and templates
	(ten or fewer lines in length), you do both of the following:
	.
	   a) Give prominent notice with each copy of the object code that the
	   Library is used in it and that the Library and its use are
	   covered by this License.
	.
	   b) Accompany the object code with a copy of the GNU GPL and this license
	   document.
	.
	  4. Combined Works.
	.
	  You may convey a Combined Work under terms of your choice that,
	taken together, effectively do not restrict modification of the
	portions of the Library contained in the Combined Work and reverse
	engineering for debugging such modifications, if you also do each of
	the following:
	.
	   a) Give prominent notice with each copy of the Combined Work that
	   the Library is used in it and that the Library and its use are
	   covered by this License.
	.
	   b) Accompany the Combined Work with a copy of the GNU GPL and this license
	   document.
	.
	   c) For a Combined Work that displays copyright notices during
	   execution, include the copyright notice for the Library among
	   these notices, as well as a reference directing the user to the
	   copies of the GNU GPL and this license document.
	.
	   d) Do one of the following:
	.
	       0) Convey the Minimal Corresponding Source under the terms of this
	       License, and the Corresponding Application Code in a form
	       suitable for, and under terms that permit, the user to
	       recombine or relink the Application with a modified version of
	       the Linked Version to produce a modified Combined Work, in the
	       manner specified by section 6 of the GNU GPL for conveying
	       Corresponding Source.
	.
	       1) Use a suitable shared library mechanism for linking with the
	       Library.  A suitable mechanism is one that (a) uses at run time
	       a copy of the Library already present on the user's computer
	       system, and (b) will operate properly with a modified version
	       of the Library that is interface-compatible with the Linked
	       Version.
	.
   	e) Provide Installation Information, but only if you would otherwise
	   be required to provide such information under section 6 of the
	   GNU GPL, and only to the extent that such information is
	   necessary to install and execute a modified version of the
	   Combined Work produced by recombining or relinking the
	   Application with a modified version of the Linked Version. (If
	   you use option 4d0, the Installation Information must accompany
	   the Minimal Corresponding Source and Corresponding Application
	   Code. If you use option 4d1, you must provide the Installation
	   Information in the manner specified by section 6 of the GNU GPL
	   for conveying Corresponding Source.)
	.
	  5. Combined Libraries.
	.
	  You may place library facilities that are a work based on the
	Library side by side in a single library together with other library
	facilities that are not Applications and are not covered by this
	License, and convey such a combined library under terms of your
	choice, if you do both of the following:
	.
	   a) Accompany the combined library with a copy of the same work based
	   on the Library, uncombined with any other library facilities,
	   conveyed under the terms of this License.
	.
	   b) Give prominent notice with the combined library that part of it
	   is a work based on the Library, and explaining where to find the
	   accompanying uncombined form of the same work.
	.
	  6. Revised Versions of the GNU Lesser General Public License.
	.
	  The Free Software Foundation may publish revised and/or new versions
	of the GNU Lesser General Public License from time to time. Such new
	versions will be similar in spirit to the present version, but may
	differ in detail to address new problems or concerns.
	.
	  Each version is given a distinguishing version number. If the
	Library as you received it specifies that a certain numbered version
	of the GNU Lesser General Public License "or any later version"
	applies to it, you have the option of following the terms and
	conditions either of that published version or of any later version
	published by the Free Software Foundation. If the Library as you
	received it does not specify a version number of the GNU Lesser
	General Public License, you may choose any version of the GNU Lesser
	General Public License ever published by the Free Software Foundation.
	.
	  If the Library as you received it specifies that a proxy can decide
	whether future versions of the GNU Lesser General Public License shall
	apply, that proxy's public statement of acceptance of any version is
	permanent authorization for you to choose that version for the
	Library.

License: GPL-2+
	This program is free software; you can redistribute it and/or
	modify it under the terms of the GNU General Public License
	as published by the Free Software Foundation; either version 2
	of the License, or (at your option) any later version.
	.
	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
	GNU General Public License for more details.
	.
	You should have received a copy of the GNU General Public License
	along with this program; if not, write to the Free Software
	Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
	.
	On Debian systems, the complete text of the GNU General Public License
	can be found in /usr/share/common-licenses/GPL-2 file.
